/*
 * RTL-SDR CP Driver for Android, based on rtl-sdr
 * Copyright (C) 2012 by Steve Markgraf <steve@steve-m.de>
 * Copyright (C) 2012-2013 by Hoernchen <la@tfc-server.de>
 * Copyright (C) 2020  Evgeni Karalamov <ekaralamov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>
#include <jni.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "rtl-sdr.h"
#include <poll.h>
#include <sys/eventfd.h>
#include <pthread.h>

typedef struct { /* structure size must be multiple of 2 bytes */
	char magic[4];
	uint32_t tuner_type;
	uint32_t tuner_gain_count;
} dongle_info_t;

typedef struct {
    rtlsdr_dev_t *dev;

    int close_event_fd;

    int primed_event_fd;
    unsigned int priming_status;

    pthread_mutex_t commands_mutex;
} session_t;

#define FREQ_PRIMING_BIT 1u << 0u
#define SAMPLE_RATE_PRIMING_BIT 1u << 1u
#define PRIMED -1u + (1u << 2u)

static void set_priming_bit(session_t *const session, unsigned int const priming_bit) {
    if (session->priming_status == PRIMED)
        return;
    session->priming_status |= priming_bit;
    if (session->priming_status != PRIMED)
        return;
    uint64_t const event = 1;
    write(session->primed_event_fd, &event, sizeof(event));
}

typedef struct {
    session_t *session;
    struct pollfd poll_fds[2];
} data_pumping_context_t;

JNIEXPORT void JNICALL
Java_sdr_driver_cp_opening_NativeCalls_stopPumps(
    JNIEnv __unused *env,
    jobject __unused thiz,
    jlong const native_session_handle
) {
    session_t *const session = (session_t *const) native_session_handle;

    uint64_t const event = 1;
    write(session->close_event_fd, &event, sizeof(event));

	rtlsdr_cancel_async(session->dev);
}

void rtlsdr_callback(unsigned char *buf, uint32_t len, void *ctx)
{
	data_pumping_context_t *data_pumping_context = ctx;
    ssize_t write_result;
	do {
		if (poll(data_pumping_context->poll_fds, 2, -1) < 0 ||
            data_pumping_context->poll_fds[0].revents ||
            (write_result = write(data_pumping_context->poll_fds[1].fd, buf, len)) <= 0
		) {
            rtlsdr_cancel_async(data_pumping_context->session->dev);
            break;
        }
		buf += write_result;
		len -= write_result;
	} while (len);
}

static int set_gain_by_index(rtlsdr_dev_t *_dev, unsigned int index)
{
	int res = 0;
	int count = rtlsdr_get_tuner_gains(_dev, NULL);

	if (count > 0 && (unsigned int)count > index) {
		int gains[count];
		rtlsdr_get_tuner_gains(_dev, gains);

		res = rtlsdr_set_tuner_gain(_dev, gains[index]);
	}

	return res;
}

#ifdef _WIN32
#define __attribute__(x)
#pragma pack(push, 1)
#endif
struct command{
	unsigned char cmd;
	unsigned int param;
}__attribute__((packed));
#ifdef _WIN32
#pragma pack(pop)
#endif

JNIEXPORT void JNICALL
Java_sdr_driver_cp_opening_NativeCalls_pumpCommands(
	JNIEnv __unused *env,
	jobject __unused thiz,
	jlong native_session_handle,
	jint input_fd
) {
	size_t left;
	ssize_t received;
	struct command cmd;
	uint32_t tmp;
    session_t *const session = (session_t *const) native_session_handle;
	rtlsdr_dev_t *const dev = session->dev;
    struct pollfd poll_fds[] = {
        {.fd = session->close_event_fd, .events = POLLIN},
        {.fd = input_fd, .events = POLLIN}
    };

	for (;;) {
		left=sizeof(cmd);
		while (left > 0) {
            if (poll(poll_fds, 2, -1) < 0 ||
                poll_fds[0].revents ||
                (received = read(input_fd, (char *) &cmd + (sizeof(cmd) - left), left)) <= 0
			)
				return;
			left -= received;
		}
		pthread_mutex_lock(&session->commands_mutex);
		switch(cmd.cmd) {
            case 0x01:
                rtlsdr_set_center_freq(dev, ntohl(cmd.param));
                set_priming_bit(session, FREQ_PRIMING_BIT);
                break;
            case 0x02:
                rtlsdr_set_sample_rate(dev, ntohl(cmd.param));
                set_priming_bit(session, SAMPLE_RATE_PRIMING_BIT);
                break;
            case 0x03:
                rtlsdr_set_tuner_gain_mode(dev, ntohl(cmd.param));
                break;
            case 0x04:
                rtlsdr_set_tuner_gain(dev, ntohl(cmd.param));
                break;
            case 0x05:
                rtlsdr_set_freq_correction(dev, ntohl(cmd.param));
                break;
            case 0x06:
                tmp = ntohl(cmd.param);
                rtlsdr_set_tuner_if_gain(dev, tmp >> 16u, (short) (tmp & 0xffffu));
                break;
            case 0x07:
                rtlsdr_set_testmode(dev, ntohl(cmd.param));
                break;
            case 0x08:
                rtlsdr_set_agc_mode(dev, ntohl(cmd.param));
                break;
            case 0x09:
                rtlsdr_set_direct_sampling(dev, ntohl(cmd.param));
                break;
            case 0x0a:
                rtlsdr_set_offset_tuning(dev, ntohl(cmd.param));
                break;
            case 0x0b:
                rtlsdr_set_xtal_freq(dev, ntohl(cmd.param), 0);
                break;
            case 0x0c:
                rtlsdr_set_xtal_freq(dev, 0, ntohl(cmd.param));
                break;
            case 0x0d:
                set_gain_by_index(dev, ntohl(cmd.param));
                break;
            case 0x0e:
                rtlsdr_set_bias_tee(dev, (int) ntohl(cmd.param));
                break;
            default:
                break;
        }
		pthread_mutex_unlock(&session->commands_mutex);
	}
}

JNIEXPORT jlong JNICALL
Java_sdr_driver_cp_opening_NativeCalls_open(
	JNIEnv __unused *env,
	jobject __unused thiz,
	jint const devFD
) {
    session_t *session = malloc(sizeof(session_t));
    if (!session)
        return -1;
    if (pthread_mutex_init(&session->commands_mutex, NULL)) {
        free(session);
        return -1;
    }
    session->close_event_fd = eventfd(0, 0);
    if (session->close_event_fd < 0) {
        pthread_mutex_destroy(&session->commands_mutex);
        free(session);
        return -1;
    }
    session->primed_event_fd = eventfd(0, 0);
    if (session->primed_event_fd < 0) {
        close(session->close_event_fd);
        pthread_mutex_destroy(&session->commands_mutex);
        free(session);
        return -1;
    }
    if (rtlsdr_open(&session->dev, devFD)) {
        close(session->primed_event_fd);
        close(session->close_event_fd);
        pthread_mutex_destroy(&session->commands_mutex);
        free(session);
        return -1;
    }
    if (rtlsdr_reset_buffer(session->dev) < 0) {
        rtlsdr_close(session->dev);
        close(session->primed_event_fd);
        close(session->close_event_fd);
        pthread_mutex_destroy(&session->commands_mutex);
        free(session);
        return -1;
    }
    session->priming_status = 0;
    return (jlong const) session;
}

static int set_non_blocking(jint const fd) {
    int flags = fcntl(fd, F_GETFL, 0);
    return
        flags == -1 ||
        fcntl(fd, F_SETFL, ((unsigned int) flags) | (unsigned int) O_NONBLOCK) == -1;
}

static int send_dongle_info(jint const output_fd, session_t *const session) {
    int r;
    dongle_info_t dongle_info;

    memset(&dongle_info, 0, sizeof(dongle_info));
    memcpy(&dongle_info.magic, "RTL0", 4);

    pthread_mutex_lock(&session->commands_mutex);

    r = rtlsdr_get_tuner_type(session->dev);
    if (r >= 0)
        dongle_info.tuner_type = htonl(r);

    r = rtlsdr_get_tuner_gains(session->dev, NULL);
    if (r >= 0)
        dongle_info.tuner_gain_count = htonl(r);

    pthread_mutex_unlock(&session->commands_mutex);

    return write(output_fd, &dongle_info, sizeof(dongle_info)) != sizeof(dongle_info);
}

static int wait_for_priming(jint const output_fd, session_t *const session) {
    struct pollfd poll_fds[] = {
        {.fd = session->close_event_fd, .events = POLLIN},
        {.fd = output_fd, .events = 0},
        {.fd = session->primed_event_fd, .events = POLLIN}
    };
    for (;;) {
        int poll_res = poll(poll_fds, 3, /*timeout_ms =*/ 50);
        if (poll_res < 0)
            return 1;
        if (poll_res)
            return
                poll_fds[0].revents ||
                poll_fds[1].revents;

        uint32_t dummy_samples_to_trigger_pipe_error_if_closed = 0x7f7f8080;
        if (
            write(
                output_fd,
                &dummy_samples_to_trigger_pipe_error_if_closed,
                sizeof(dummy_samples_to_trigger_pipe_error_if_closed))
            <= 0)
            return 1;
    }
}

JNIEXPORT void JNICALL
Java_sdr_driver_cp_opening_NativeCalls_pumpData(
    JNIEnv __unused *env,
    jobject __unused thiz,
    jlong const native_session_handle,
    jint const output_fd
) {
    session_t *const session = (session_t *const) native_session_handle;

    if (set_non_blocking(output_fd) ||
        send_dongle_info(output_fd, session) ||
        wait_for_priming(output_fd, session)
        )
        return;

    data_pumping_context_t data_pumping_context;
    data_pumping_context.session = session;
    data_pumping_context.poll_fds[0].fd = session->close_event_fd;
    data_pumping_context.poll_fds[0].events = POLLIN;
    data_pumping_context.poll_fds[1].fd = output_fd;
    data_pumping_context.poll_fds[1].events = POLLOUT;
    rtlsdr_read_async(session->dev, rtlsdr_callback, &data_pumping_context, 2, 0);
}

JNIEXPORT void JNICALL
Java_sdr_driver_cp_opening_NativeCalls_close(
	JNIEnv __unused *env,
	jobject __unused thiz,
	jlong native_session_handle
) {
    session_t *const session = (session_t *const) native_session_handle;
    rtlsdr_close(session->dev);
    close(session->primed_event_fd);
    close(session->close_event_fd);
    pthread_mutex_destroy(&session->commands_mutex);
    free(session);
}
