The [rtl-sdr](https://osmocom.org/projects/rtl-sdr/wiki/Rtl-sdr#Software) codebase, adapted and used as a
submodule in RTL-SDR CP Driver. Licensed under [GNU General Public License v3.0](COPYING) or, at your option,
any later version.
